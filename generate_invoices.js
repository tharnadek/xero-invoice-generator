const fs = require("fs");
const fetch = require("node-fetch");

// ===================
// OPTIONS
// ===================

// set to ACCREC for Receivables or ACCPAY for Payables
const INVOICE_TYPE = "ACCREC";
// How many invoices to generate
const NUM_OF_INVOICES = 1;
// Set to the Xero Contact ID of the contact you want to target.
const CONTACT_ID = "97779bc1-a198-4c61-9985-a32e0f6b7b07";

// if true the script will attempt to post to Xero
const MAKE_HTTP_REQUEST = true;
const BEARER_TOKEN =
  "eyJhbGciOiJSUzI1NiIsImtpZCI6IjFDQUY4RTY2NzcyRDZEQzAyOEQ2NzI2RkQwMjYxNTgxNTcwRUZDMTkiLCJ0eXAiOiJKV1QiLCJ4NXQiOiJISy1PWm5jdGJjQW8xbkp2MENZVmdWY09fQmsifQ.eyJuYmYiOjE2MTM2ODA5MjgsImV4cCI6MTYxMzY4MjcyOCwiaXNzIjoiaHR0cHM6Ly9pZGVudGl0eS54ZXJvLmNvbSIsImF1ZCI6Imh0dHBzOi8vaWRlbnRpdHkueGVyby5jb20vcmVzb3VyY2VzIiwiY2xpZW50X2lkIjoiOTUzMEQxN0NFMEM5NDQyNEE4NENGNUMwOEY4QUQ4MzYiLCJzdWIiOiI1NzVhMTNmM2FjYzU1MTM0YTJlODgxOGUwOTY5ZWU4ZCIsImF1dGhfdGltZSI6MTYxMzQ5NzUyMSwieGVyb191c2VyaWQiOiJjY2FhNjlkNC04ZmU5LTRiMmUtOTE0Ny0xNWU1YzVhZjNmMDQiLCJnbG9iYWxfc2Vzc2lvbl9pZCI6ImE0MzNhOTM1NGY2YzRmYzlhZDlmMmZkMWFkODNiZjk2IiwianRpIjoiZjVjNDJmMzg2NDJhMTVjMmYxOGMzOTY4YjU0ZjZiYTMiLCJhdXRoZW50aWNhdGlvbl9ldmVudF9pZCI6IjYwYjg2MmFlLTEyMjktNDVlOS1hOTE4LWE0YzE1M2YyZGM1ZSIsInNjb3BlIjpbImVtYWlsIiwicHJvZmlsZSIsIm9wZW5pZCIsImFjY291bnRpbmcuc2V0dGluZ3MiLCJhY2NvdW50aW5nLnRyYW5zYWN0aW9ucyIsImFjY291bnRpbmcuY29udGFjdHMiLCJvZmZsaW5lX2FjY2VzcyJdfQ.YE4dCU0OvXLdkTRfo7cG6UiTYZ5vVBS61IvRpYpzezrqBlvh1Dv0plNdV1jLOUJfWpTzWl5WMwUqw_jGaH5wcmveHZbPA9hg7G9Z4fvj_q4S7bxhiWp2fvEig3fz1NWoYJ7DcAleVa_lQmBYuLseXXp0ynHFDL2WtbqlA316dGUac7_A48cBFCtf6TAe0d7ny4qdV3NslAd_UC_d9Sygnjo26krPlqd2UWa-4iY5sRzF3vJoZWMaizYCGzDgsq5ZsTbPzQ1n9RZ7S1wPwtWtasN5sLrvr8IO_bEoPXw24bWNiiqJtEktMFz0S6PQ510Q_6dAhEkod3kUJoWD0BAZFg";
const TENANT_ID = "9148f368-5f02-40e2-b6b1-cb76e659208c";

// if true the script will output to a file
const OUTPUT_TO_FILE = true;
const OUTPUT_FILE_NAME = "output.json";

// ===================
// START SCRIPT
// ===================

// Words in the dict are randomly put into the description and reference fields
// for tester-readable randomness
const dict = [
  "Tires",
  "Wheels",
  "Website",
  "Shoes",
  "Hats",
  "Football",
  "Sweatshirt",
  "Consulting",
  "Animation",
  "Task",
  "History",
  "Textbook",
  "Keyboard",
  "Phone",
  "Axle",
  "Skis",
  "Helmet",
  "Lamp",
  "Horse",
  "Fish",
];

const invoices = [];
const date = new Date();
const dueDate = new Date(date.setMonth(date.getMonth() + 1));
const referenceNumber = 10000 + Math.floor(Math.random() * 80000);
const referenceWord = dict[Math.floor(Math.random() * dict.length)];

const generateInvoice = (i) => ({
  Type: INVOICE_TYPE,
  Contact: {
    ContactID: CONTACT_ID,
  },
  LineItems: [
    {
      Description: dict[Math.floor(Math.random() * dict.length)],
      Quantity: quantity,
      UnitAmount: unitAmount,
      AccountCode: "200",
      TaxType: "NONE",
      LineAmount: lineAmount,
    },
  ],
  Date: date.toISOString(),
  DueDate: dueDate.toISOString(),
  Reference: `REF-${referenceWord}-${referenceNumber + i}`,
  InvoiceNumber: `INV-${referenceWord}-${referenceNumber + i}`,
  CurrencyCode: "CAD",
  Status: "AUTHORISED",
});

// start script
for (i = 0; i < NUM_OF_INVOICES; i++) {
  quantity = 1 + Math.floor(Math.random() * 4);
  unitAmount = 1 + Math.floor(Math.random() * 40);
  lineAmount = quantity * unitAmount;
  invoices.push(generateInvoice(i));
}

// convert JSON object to string
const data = JSON.stringify(
  {
    Invoices: invoices,
  },
  null,
  2
);

if (OUTPUT_TO_FILE) {
  // write JSON string to a file
  fs.writeFile(OUTPUT_FILE_NAME, data, (err) => {
    if (err) {
      throw err;
    }
    console.log(`Saved JSON Data to ${OUTPUT_FILE_NAME}`);
  });
}

if (MAKE_HTTP_REQUEST) {
  let url = "https://api.xero.com/api.xro/2.0/Invoices";

  let options = {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
      "Xero-Tenant-Id": TENANT_ID,
      Authorization: "Bearer " + BEARER_TOKEN,
    },
    body: data,
  };

  fetch(url, options)
    .then((res) => res.json())
    .then((json) => console.log(json))
    .catch((err) => console.error("error:" + err));
}
