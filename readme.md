# Purpose

This script is for generating random invoices for Xero

# Usage

1. command line: `npm i`
2. get a bearer token and tenant id from Xero
3. edit `generate_invoices.js` and insert your bearer token, tenant id, and adjust any options
4. ensure you set the `MAKE_HTTP_REQUEST` and `OUTPUT_TO_FILE` options as preferred
5. command line: `node generate_invoices.js`
